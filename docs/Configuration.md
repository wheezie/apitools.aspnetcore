# Configuration
**Configuration can be put in any config file as long as it gets injected by DI. Configuration should be put in the 'ApiTools' section of your configuration, the following subsections should be derived from it.**

## Security
### Jwt
In order to use Json Web Tokens (JWT) as your backend authentication you will need to configure your issuers, audiences and the secret.

#### Algorithm
The securing algorithm used to sign new tokens.

#### Audiences
List of validated token audiences.
The first audience will used when signing tokens.

#### Issuers
List of validated token issuers.
The first issuer will used when signing tokens.

#### Secret
Symmetric key for Hmac-like algorithms.

*Example:*
```json
{
    "ApiTools": {
        "Security": {
            "Jwt": {
                "Algorithm": "HS256",
                "Audiences": [
                    "http://localhost:4200" // Used when signing tokens
                ],
                "Issues": [
                    "https://localhost:5001", // Used when signing tokens.
                    "http://localhost:5000"
                ],
                "Secret": "I_should#be-more@secure"
            }
        }
    }
}
```
### Password
#### Factor
BCrypt salting factor

#### Enhanced
Specify if password should be hashed before BCrypting (adds security for smaller passwords).

#### Algorithm
SHA algorithm to hash password with, must be one of the following:
* SHA256
* SHA384
* SHA512

*Example:*
```json
{
    "ApiTools": {
        "Security": {
            "Password": {
                "Factor": 12,
                "Enhanced": true,
                "Algorithm": "SHA512"
            }
        }
    }
}
```

## Repositories
**Repositories sometimes require configuration for stuff like validation etc.**

### Accounts
#### Username
**Minimum**: [int] Minimum length (1-32) - Default = 4

**Maximum**: [int] Maximum length (1-32) - Default = 32

#### FirstName
**Minimum**: [int] Minimum length (1-32) - Default = 2

**Maximum**: [int] Maximum length (1-32) - Default = 32

#### LastName
**Minimum**: [int] Minimum length (1-32) - Default = 2

**Maximum**: [int] Maximum length (1-32) - Default = 32

#### Password
**Minimum**: [int] Minimum length (4+) - Default = 6

**Number**: [boolean] Force number(s) - Default = true

**SpecialCharacter**: [boolean] Force special character(s) - Default = true
*Example*:
```json
{
    "ApiTools": {
        "Repositories": {
            "Accounts": {
                "Username": {
                    "Minimum": 2,
                    "Maximum": 32
                },
                "FirstName": {
                    "Minimum": 2,
                    "Maximum": 32
                },
                "LastName": {
                    "Minimum": 2,
                    "Maximum": 32
                },
                "Password": {
                    "Minimum": 8,
                    "Number": true,
                    "SpecialCharacter": false
                }
            }
        }
    }
}
``` 

### Roles
#### Name
**Minimum**: [int] Minimum length (1-24)

**Maximum**: [int] Maximum length (1-24)
*Example*:
```json
{
    "ApiTools": {
        "Repositories": {
            "Roles": {
                "Name": {
                    "Minimum": 2,
                    "Maximum": 24
                }
            }
        }
    }
}
``` 

### Albums
#### Title
**Minimum**: [int] Minimum length (1-48)

**Maximum**: [int] Maximum length (1-48)

#### Description
**Minimum**: [int] Minimum length (1-65535)

**Maximum**: [int] Maximum length (1-65535)
*Example*:
```json
{
    "ApiTools": {
        "Repositories": {
            "Albums": {
                "Title": {
                    "Minimum": 1,
                    "Maximum": 48
                },
                "Description": {
                    "Minimum": 1,
                    "Maximum": 65535
                }
            }
        }
    }
}
``` 

### BlogPosts
#### Title
**Minimum**: [int] Minimum length (1-48)

**Maximum**: [int] Maximum length (1-48)

#### Content
**Minimum**: [int] Minimum length (1-65535)

**Maximum**: [int] Maximum length (1-65535)
*Example*:
```json
{
    "ApiTools": {
        "Repositories": {
            "BlogPosts": {
                "Title": {
                    "Minimum": 1,
                    "Maximum": 48
                },
                "Description": {
                    "Minimum": 1,
                    "Maximum": 65535
                }
            }
        }
    }
}
