using System;
using Microsoft.EntityFrameworkCore;

using ApiTools.AspNetCore.EF.Models;

namespace ApiTools.AspNetCore.EF
{
    public class AppDbContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountRole> AccountRoles { get; set; }
        public DbSet<AccountPermission> AccountPermissions { get; set; }
        public DbSet<RegistrationToken> RegistrationTokens { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<Picture> AlbumPictures { get; set; }
        public DbSet<BlogPost> BlogPosts { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
#region MB_Account
            modelBuilder.Entity<Account>()
                .Ignore(a => a.FullName);

            modelBuilder.Entity<Account>()
                .Property(a => a.Email)
                .HasColumnType("varchar(255)")
                .IsRequired();
            modelBuilder.Entity<Account>()
                .HasIndex(a => a.Email)
                .IsUnique();

            modelBuilder.Entity<Account>()
                .Property(a => a.Username)
                .HasColumnType("varchar(32)")
                .HasMaxLength(32)
                .IsRequired();
            modelBuilder.Entity<Account>()
                .HasIndex(a => a.Username)
                .IsUnique();

            modelBuilder.Entity<Account>()
                .Property(a => a.FirstName)
                .HasColumnType("varchar(32)")
                .HasMaxLength(32)
                .IsRequired();

            modelBuilder.Entity<Account>()
                .Property(a => a.LastName)
                .HasColumnType("varchar(32)")
                .HasMaxLength(32)
                .IsRequired();

            modelBuilder.Entity<Account>()
                .Property(a => a.Description)
                .HasColumnType("varchar(255)")
                .HasMaxLength(255);

            modelBuilder.Entity<Account>()
                .Property(a => a.Password)
                .HasColumnType("varchar(80)")
                .HasMaxLength(80)
                .IsRequired();

            modelBuilder.Entity<Account>()
                .Property(a => a.ExpireTokensBefore)
                .HasConversion(d => d, d => DateTime.SpecifyKind(d, DateTimeKind.Utc));

            modelBuilder.Entity<Account>()
                .Property(a => a.Created)
                .HasConversion(d => d, d => DateTime.SpecifyKind(d, DateTimeKind.Utc));

            modelBuilder.Entity<Account>()
                .Property(a => a.BlockedDate)
                .HasConversion(d => d, d => DateTime.SpecifyKind(d, DateTimeKind.Utc));
#endregion
#region MB_AccountRole
            modelBuilder.Entity<AccountRole>()
                .Property(a => a.Name)
                .HasMaxLength(24)
                .HasColumnType("varchar(24)")
                .IsRequired();
            
            modelBuilder.Entity<AccountRole>()
                .HasMany(r => r.Permissions);

            modelBuilder.Entity<AccountRole>()
                .Property(r => r.Changed)
                .HasConversion(d => d, d => DateTime.SpecifyKind(d, DateTimeKind.Utc));
#endregion
#region MB_AccountPermission
            modelBuilder.Entity<AccountPermission>()
                .Property(p => p.Id)
                .HasMaxLength(48)
                .HasColumnType("varchar(48)");
            modelBuilder.Entity<AccountPermission>()
                .HasKey(l => new { l.Id, l.AccountRoleId });
            
            modelBuilder.Entity<AccountPermission>()
                .Property(p => p.Name)
                .HasMaxLength(48)
                .HasColumnType("varchar(48)")
                .IsRequired();
            
            modelBuilder.Entity<AccountPermission>()
                .Property(p => p.Description)
                .HasMaxLength(255)
                .HasColumnType("varchar(255)");
#endregion
#region MB_RegistrationToken
            modelBuilder.Entity<RegistrationToken>()
                .Property(t => t.Created)
                .HasConversion(d => d, d => DateTime.SpecifyKind(d, DateTimeKind.Utc));
#endregion
#region MB_Album
            modelBuilder.Entity<Album>()
                .Property(a => a.Title)
                .HasMaxLength(48)
                .HasColumnType("varchar(48)")
                .IsRequired();
            
            modelBuilder.Entity<Album>()
                .Property(a => a.Description)
                .HasMaxLength(65535)
                .HasColumnType("TEXT");
            
            modelBuilder.Entity<Album>()
                .Property(a => a.Published)
                .HasConversion(d => d, d => DateTime.SpecifyKind(d, DateTimeKind.Utc));
#endregion
#region MB_Picture
            modelBuilder.Entity<Picture>()
                .Property(a => a.Deleted)
                .HasDefaultValue(false);

            modelBuilder.Entity<Picture>()
                .Property(a => a.Hidden)
                .HasDefaultValue(false);

            modelBuilder.Entity<Picture>()
                .Property(a => a.Path)
                .HasMaxLength(255)
                .HasColumnType("varchar(255)")
                .IsRequired();

            modelBuilder.Entity<Picture>()
                .HasOne(p => p.Uploader)
                .WithOne(a => a.Image)
                .HasForeignKey<Picture>(p => p.UploaderForeignKey);

            modelBuilder.Entity<Picture>()
                .Property(a => a.Uploaded)
                .HasConversion(d => d, d => DateTime.SpecifyKind(d, DateTimeKind.Utc));
#endregion
#region MB_BlogPost            
            modelBuilder.Entity<BlogPost>()
                .Property(p => p.Published)
                .HasConversion(d => d, d => DateTime.SpecifyKind(d, DateTimeKind.Utc));

            modelBuilder.Entity<BlogPost>()
                .Property(p => p.Modified)
                .HasConversion(d => d, d => DateTime.SpecifyKind(d, DateTimeKind.Utc));

            modelBuilder.Entity<BlogPost>()
                .Property(p => p.Public)
                .HasDefaultValue(true);

            modelBuilder.Entity<BlogPost>()
                .Property(p => p.Title)
                .HasMaxLength(48)
                .HasColumnType("varchar(48)")
                .IsRequired();

            modelBuilder.Entity<BlogPost>()
                .Property(p => p.Content)
                .HasMaxLength(65535)
                .HasColumnType("TEXT")
                .IsRequired();
#endregion
        }
    }
}