﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ApiTools.AspNetCore.EF.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccountRoles",
                columns: table => new
                {
                    Id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "varchar(24)", maxLength: 24, nullable: false),
                    Default = table.Column<bool>(nullable: false),
                    Changed = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AccountPermission",
                columns: table => new
                {
                    Id = table.Column<string>(type: "varchar(48)", maxLength: 48, nullable: false),
                    AccountRoleId = table.Column<uint>(nullable: false),
                    Name = table.Column<string>(type: "varchar(48)", maxLength: 48, nullable: false),
                    Description = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountPermission", x => new { x.Id, x.AccountRoleId });
                    table.ForeignKey(
                        name: "FK_AccountPermission_AccountRoles_AccountRoleId",
                        column: x => x.AccountRoleId,
                        principalTable: "AccountRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    Id = table.Column<ulong>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<uint>(nullable: true),
                    FirstName = table.Column<string>(type: "varchar(24)", maxLength: 24, nullable: false),
                    LastName = table.Column<string>(type: "varchar(24)", maxLength: 24, nullable: false),
                    Email = table.Column<string>(type: "varchar(255)", nullable: false),
                    Description = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    Password = table.Column<byte[]>(fixedLength: true, maxLength: 64, nullable: false),
                    Salt = table.Column<byte[]>(fixedLength: true, maxLength: 8, nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    BlockedDate = table.Column<DateTime>(nullable: false),
                    Blocked = table.Column<bool>(nullable: false),
                    ExpireTokensBefore = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Accounts_AccountRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AccountRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Albums",
                columns: table => new
                {
                    Id = table.Column<ulong>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(type: "varchar(48)", maxLength: 48, nullable: false),
                    Description = table.Column<string>(type: "TEXT", maxLength: 65535, nullable: true),
                    Published = table.Column<DateTime>(nullable: false),
                    PublisherId = table.Column<ulong>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Albums", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Albums_Accounts_PublisherId",
                        column: x => x.PublisherId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RegistrationTokens",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatorId = table.Column<ulong>(nullable: true),
                    RegisteredId = table.Column<ulong>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RegistrationTokens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RegistrationTokens_Accounts_CreatorId",
                        column: x => x.CreatorId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RegistrationTokens_Accounts_RegisteredId",
                        column: x => x.RegisteredId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AlbumPictures",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Path = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false),
                    UploaderForeignKey = table.Column<ulong>(nullable: false),
                    Uploaded = table.Column<DateTime>(nullable: false),
                    Hidden = table.Column<bool>(nullable: false, defaultValue: false),
                    Deleted = table.Column<bool>(nullable: false, defaultValue: false),
                    AlbumId = table.Column<ulong>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AlbumPictures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AlbumPictures_Albums_AlbumId",
                        column: x => x.AlbumId,
                        principalTable: "Albums",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AlbumPictures_Accounts_UploaderForeignKey",
                        column: x => x.UploaderForeignKey,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BlogPosts",
                columns: table => new
                {
                    Id = table.Column<ulong>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Published = table.Column<DateTime>(nullable: false),
                    PublisherId = table.Column<ulong>(nullable: true),
                    Modified = table.Column<DateTime>(nullable: false),
                    ModifierId = table.Column<ulong>(nullable: true),
                    Title = table.Column<string>(type: "varchar(48)", maxLength: 48, nullable: false),
                    Content = table.Column<string>(type: "TEXT", maxLength: 65535, nullable: false),
                    ImageId = table.Column<Guid>(nullable: true),
                    Public = table.Column<bool>(nullable: false, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogPosts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BlogPosts_AlbumPictures_ImageId",
                        column: x => x.ImageId,
                        principalTable: "AlbumPictures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BlogPosts_Accounts_ModifierId",
                        column: x => x.ModifierId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BlogPosts_Accounts_PublisherId",
                        column: x => x.PublisherId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountPermission_AccountRoleId",
                table: "AccountPermission",
                column: "AccountRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_Email",
                table: "Accounts",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_RoleId",
                table: "Accounts",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AlbumPictures_AlbumId",
                table: "AlbumPictures",
                column: "AlbumId");

            migrationBuilder.CreateIndex(
                name: "IX_AlbumPictures_UploaderForeignKey",
                table: "AlbumPictures",
                column: "UploaderForeignKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Albums_PublisherId",
                table: "Albums",
                column: "PublisherId");

            migrationBuilder.CreateIndex(
                name: "IX_BlogPosts_ImageId",
                table: "BlogPosts",
                column: "ImageId");

            migrationBuilder.CreateIndex(
                name: "IX_BlogPosts_ModifierId",
                table: "BlogPosts",
                column: "ModifierId");

            migrationBuilder.CreateIndex(
                name: "IX_BlogPosts_PublisherId",
                table: "BlogPosts",
                column: "PublisherId");

            migrationBuilder.CreateIndex(
                name: "IX_RegistrationTokens_CreatorId",
                table: "RegistrationTokens",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_RegistrationTokens_RegisteredId",
                table: "RegistrationTokens",
                column: "RegisteredId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountPermission");

            migrationBuilder.DropTable(
                name: "BlogPosts");

            migrationBuilder.DropTable(
                name: "RegistrationTokens");

            migrationBuilder.DropTable(
                name: "AlbumPictures");

            migrationBuilder.DropTable(
                name: "Albums");

            migrationBuilder.DropTable(
                name: "Accounts");

            migrationBuilder.DropTable(
                name: "AccountRoles");
        }
    }
}
