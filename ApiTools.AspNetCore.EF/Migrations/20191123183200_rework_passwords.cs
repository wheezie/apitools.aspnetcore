﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ApiTools.AspNetCore.EF.Migrations
{
    public partial class rework_passwords : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Salt",
                table: "Accounts");

            migrationBuilder.AlterColumn<string>(
                name: "Password",
                table: "Accounts",
                type: "varchar(80)",
                maxLength: 80,
                nullable: false,
                oldClrType: typeof(byte[]),
                oldType: "binary(64)",
                oldFixedLength: true,
                oldMaxLength: 64);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte[]>(
                name: "Password",
                table: "Accounts",
                type: "binary(64)",
                fixedLength: true,
                maxLength: 64,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(80)",
                oldMaxLength: 80);

            migrationBuilder.AddColumn<byte[]>(
                name: "Salt",
                table: "Accounts",
                type: "binary(8)",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                defaultValue: new byte[] {  });
        }
    }
}
