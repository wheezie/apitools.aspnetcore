using System;

namespace ApiTools.AspNetCore.EF.Models
{
    public class RegistrationToken
    {
        /// <summary>
        /// Registration token identifier.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Account creating the token.
        /// </summary>
        public Account Creator { get; set; }
        /// <summary>
        /// Account accepting the token.
        /// </summary>
        public Account Registered { get; set; }
        /// <summary>
        /// Token creation date
        /// </summary>
        public DateTime Created { get; set; }
    }
}