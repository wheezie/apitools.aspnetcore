using System;
using System.Collections.Generic;

namespace ApiTools.AspNetCore.EF.Models
{
    public class AccountRole
    {
        /// <summary>
        /// Role identifier
        /// </summary>
        public uint Id { get; set; }
        /// <summary>
        /// Role public facing name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Default role
        /// </summary>
        public bool Default { get; set; }
        /// <summary>
        /// Role change timestamp
        /// </summary>
        public DateTime Changed { get; set; }
        /// <summary>
        /// Permissions list
        /// </summary>
        public List<AccountPermission> Permissions { get; set; }
            = new List<AccountPermission>();
    }
}