using System;

namespace ApiTools.AspNetCore.EF.Models
{
    public class BlogPost
    {
        /// <summary>
        /// Post identifier
        /// </summary>
        public ulong Id { get; set; }
        /// <summary>
        /// Published date
        /// </summary>
        public DateTime Published { get; set; }
        /// <summary>
        /// Publisher account
        /// </summary>
        public Account Publisher { get; set; }
        /// <summary>
        /// Modified date
        /// </summary>
        public DateTime Modified { get; set; }
        /// <summary>
        /// Modified account
        /// </summary>
        public Account Modifier { get; set; }
        /// <summary>
        /// Post title
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Content post
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// Picture
        /// </summary>
        public Picture Image { get; set; }
        /// <summary>
        /// Public viewable state
        /// </summary>
        public bool Public { get; set; }
    }
}