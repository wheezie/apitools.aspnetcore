using System;

namespace ApiTools.AspNetCore.EF.Models
{
    public class Picture
    {
        /// <summary>
        /// Picture identifier
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Relative image path
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// Uploader account
        /// </summary>
        public Account Uploader { get; set; }
        /// <summary>
        /// Uploader foreign key
        /// </summary>
        public ulong UploaderForeignKey { get; set; }
        /// <summary>
        /// Uploaded date
        /// </summary>
        public DateTime Uploaded { get; set; }
        /// <summary>
        /// Hide a picture from the album
        /// </summary>
        public bool Hidden { get; set; }
        /// <summary>
        /// State of deletion
        /// </summary>
        public bool Deleted { get; set; }
    }
}