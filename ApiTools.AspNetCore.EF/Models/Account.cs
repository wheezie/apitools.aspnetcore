using System;

namespace ApiTools.AspNetCore.EF.Models
{
    public class Account
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        public ulong Id { get; set; }
        /// <summary>
        /// Role (permissions)
        /// </summary>
        public AccountRole Role { get; set; }
        /// <summary>
        /// First name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Last name
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Preferred username
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Corresponding email address
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Profile image
        /// </summary>
        public Picture Image { get; set; }
        /// <summary>
        /// Account description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Password hash
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Account creation date
        /// </summary>
        public DateTime Created { get; set; }
        /// <summary>
        /// Blocked date (NULL if not blocked)
        /// </summary>
        public DateTime BlockedDate { get; set; }
        /// <summary>
        /// Blocked state (default = false)
        /// </summary>
        public bool Blocked { get; set; }
        /// <summary>
        /// Expires JWT tokens generated before this date
        /// </summary>
        public DateTime ExpireTokensBefore { get; set; }

        /// <summary>
        /// Retrieve the full name of the account
        /// </summary>
        public string FullName 
        {
            get { return $"{this.FirstName} {this.LastName}"; }
        }

        /// <summary>
        /// Strips all the security credentials from the account instance
        /// </summary>
        public void StripCredentials()
        {
            this.Password = null;
        }
    }
}