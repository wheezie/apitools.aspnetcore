namespace ApiTools.AspNetCore.EF.Models
{
    public class AccountPermission
    {
        /// <summary>
        /// Identifier
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Permission's role identifier
        /// </summary>
        public uint AccountRoleId { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }
    }
}