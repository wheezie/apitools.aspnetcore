using System;
using System.Collections.Generic;

namespace ApiTools.AspNetCore.EF.Models
{
    public class Album
    {
        /// <summary>
        /// Album identifier
        /// </summary>
        public ulong Id { get; set; }
        /// <summary>
        /// Title
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Description describing the contents of the album.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Published date
        /// </summary>
        public DateTime Published { get; set; }
        /// <summary>
        /// Publishing account
        /// </summary>
        public Account Publisher { get; set; }
        /// <summary>
        /// Picture list
        /// </summary>
        public List<Picture> Pictures { get; set; }
            = new List<Picture>();
    }
}