using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace ApiTools.AspNetCore.EF
{
    public class AppDbContextFactory : IDesignTimeDbContextFactory<AppDbContext>
    {
        public AppDbContext CreateDbContext(string[] args)
        {
            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>()
                .UseMySql(config.GetConnectionString("Mysql"),
                          b => b.MigrationsAssembly("ApiTools.AspNetCore.EF"));

            return new AppDbContext(optionsBuilder.Options);
        }
    }
}