using System.Linq;
using System; 
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Xunit;

namespace ApiTools.AspNetCore.Tests
{
    public partial class ApiTokenHandlerTests
    {
        [Fact]
        public void Configuration_Decode_valid()
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim("sub", "1234567890"),
                new Claim("name", "John Doe"),
                new Claim("iat", "1516239022")
            };
            var token = this._handler.DecodeToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c");
            Assert.All(token.Claims, c => claims.Contains(c));
        }

        [Fact]
        public void Configuration_Decode_valid_validated()
        {
            var identity = new ClaimsIdentity(
                new Claim[]
                {
                    new Claim(ClaimTypes.Sid, "1"),
                    new Claim(ClaimTypes.Role, "1"),
                });
            string jwtToken = this._handler.CreateToken(identity, 1);

            JwtSecurityToken token = this._handler.DecodeToken(jwtToken, true);
            Assert.All(identity.Claims, c => token.Claims.Contains(c));
        }

        [Theory]
        [InlineData("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c")]
        [InlineData("invalid")]
        public void Configuration_Decode_verify_fail(string jwtToken)
        {
            JwtSecurityToken token = this._handler.DecodeToken(jwtToken, true);

            Assert.Null(token);
        }
    }
}