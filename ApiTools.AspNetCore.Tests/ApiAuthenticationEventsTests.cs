using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;

namespace ApiTools.AspNetCore.Tests
{
    public class ApiAuthenticationEventsTests : IDisposable
    {
        private readonly XUnitDbContext _dbContext;
        private readonly ApiAuthenticationEvents _events;
        private readonly HttpContext _context;

        public ApiAuthenticationEventsTests()
        {
            this._dbContext = new XUnitDbContext();

            this._events = new ApiAuthenticationEvents(_dbContext.DbContext);
            this._context = new DefaultHttpContext();
        }

        [Fact]
        public async void TokenValidated_Success_valid()
        {
            ClaimsPrincipal principal = new ClaimsPrincipal();
            principal.AddIdentity(new ClaimsIdentity(
                new Claim[]
                {
                    new Claim(ClaimTypes.Sid, "1"),
                    new Claim(ClaimTypes.Role, "1"),
                    new Claim("iat", Convert.ToUInt32(DateTime.UtcNow.Subtract(new DateTime(1970,1,1,0,0,0,DateTimeKind.Utc)).TotalSeconds).ToString())
                }));
            this._context.User = principal;
            var mockObject = new Mock<AuthenticationScheme>(JwtBearerDefaults.AuthenticationScheme, JwtBearerDefaults.AuthenticationScheme, typeof(JwtBearerHandler));
            TokenValidatedContext context = new TokenValidatedContext(this._context, mockObject.Object, new JwtBearerOptions());
            context.Principal = principal;
            await _events.TokenValidated(context);

            Assert.True(context.Result.Succeeded);
        }

        
        [Theory]
        [MemberData(nameof(getInvalidClaims))]
        public async void TokenValidated_Fail_invalidclaims(Claim[] claims)
        {
            ClaimsPrincipal principal = new ClaimsPrincipal();
            principal.AddIdentity(new ClaimsIdentity(claims));
            this._context.User = principal;
            var mockObject = new Mock<AuthenticationScheme>(JwtBearerDefaults.AuthenticationScheme, JwtBearerDefaults.AuthenticationScheme, typeof(JwtBearerHandler));
            TokenValidatedContext context = new TokenValidatedContext(this._context, mockObject.Object, new JwtBearerOptions());
            context.Principal = principal;
            await _events.TokenValidated(context);

            Assert.False(context.Result.Succeeded);
        }

        [Fact]
        public async void TokenValidated_Fail_blocked()
        {
            var account = this._dbContext.DbContext.Accounts
                .First(a => a.Id == 1);
            account.Blocked = true;
            this._dbContext.DbContext.Update(account);
            this._dbContext.DbContext.SaveChanges();

            ClaimsPrincipal principal = new ClaimsPrincipal();
            principal.AddIdentity(new ClaimsIdentity(
                new Claim[]
                {
                    new Claim(ClaimTypes.Sid, "1"),
                    new Claim(ClaimTypes.Role, "1"),
                    new Claim("iat", Convert.ToUInt32(DateTime.UtcNow.Subtract(new DateTime(1970,1,1,0,0,0,DateTimeKind.Utc)).TotalSeconds).ToString())
                }));
            this._context.User = principal;
            var mockObject = new Mock<AuthenticationScheme>(JwtBearerDefaults.AuthenticationScheme, JwtBearerDefaults.AuthenticationScheme, typeof(JwtBearerHandler));
            TokenValidatedContext context = new TokenValidatedContext(this._context, mockObject.Object, new JwtBearerOptions());
            context.Principal = principal;
            await _events.TokenValidated(context);

            Assert.False(context.Result.Succeeded);
        }

        [Fact]
        public async void TokenValidated_Fail_expiredbefore()
        {
            var account = this._dbContext.DbContext.Accounts
                .First(a => a.Id == 1);
            account.ExpireTokensBefore = DateTime.UtcNow.AddDays(1);
            this._dbContext.DbContext.Update(account);
            this._dbContext.DbContext.SaveChanges();

            ClaimsPrincipal principal = new ClaimsPrincipal();
            principal.AddIdentity(new ClaimsIdentity(
                new Claim[]
                {
                    new Claim(ClaimTypes.Sid, "1"),
                    new Claim(ClaimTypes.Role, "1"),
                    new Claim("iat", Convert.ToUInt32(DateTime.UtcNow.Subtract(new DateTime(1970,1,1,0,0,0,DateTimeKind.Utc)).TotalSeconds).ToString())
                }));
            this._context.User = principal;
            var mockObject = new Mock<AuthenticationScheme>(JwtBearerDefaults.AuthenticationScheme, JwtBearerDefaults.AuthenticationScheme, typeof(JwtBearerHandler));
            TokenValidatedContext context = new TokenValidatedContext(this._context, mockObject.Object, new JwtBearerOptions());
            context.Principal = principal;
            await _events.TokenValidated(context);

            Assert.False(context.Result.Succeeded);
        }

        [Theory]
        [InlineData(2)]
        [InlineData(0)]
        public async void TokenValidated_Fail_rolefails(uint roleId)
        {
            var account = this._dbContext.DbContext.Accounts
                .First(a => a.Id == 1);
            account.Role = this._dbContext.DbContext.AccountRoles.FirstOrDefault(r => r.Id == roleId);
            this._dbContext.DbContext.Update(account);
            this._dbContext.DbContext.SaveChanges();

            ClaimsPrincipal principal = new ClaimsPrincipal();
            principal.AddIdentity(new ClaimsIdentity(
                new Claim[]
                {
                    new Claim(ClaimTypes.Sid, "1"),
                    new Claim(ClaimTypes.Role, "1"),
                    new Claim("iat", Convert.ToUInt32(DateTime.UtcNow.Subtract(new DateTime(1970,1,1,0,0,0,DateTimeKind.Utc)).TotalSeconds).ToString())
                }));
            this._context.User = principal;
            var mockObject = new Mock<AuthenticationScheme>(JwtBearerDefaults.AuthenticationScheme, JwtBearerDefaults.AuthenticationScheme, typeof(JwtBearerHandler));
            TokenValidatedContext context = new TokenValidatedContext(this._context, mockObject.Object, new JwtBearerOptions());
            context.Principal = principal;
            await _events.TokenValidated(context);

            Assert.False(context.Result.Succeeded);
        }

        [Fact]
        public async void TokenValidated_Fail_non_existant()
        {
            ClaimsPrincipal principal = new ClaimsPrincipal();
            principal.AddIdentity(new ClaimsIdentity(
                new Claim[]
                {
                    new Claim(ClaimTypes.Sid, "0"),
                    new Claim(ClaimTypes.Role, "1"),
                    new Claim("iat", Convert.ToUInt32(DateTime.UtcNow.Subtract(new DateTime(1970,1,1,0,0,0,DateTimeKind.Utc)).TotalSeconds).ToString())
                }));
            this._context.User = principal;
            var mockObject = new Mock<AuthenticationScheme>(JwtBearerDefaults.AuthenticationScheme, JwtBearerDefaults.AuthenticationScheme, typeof(JwtBearerHandler));
            TokenValidatedContext context = new TokenValidatedContext(this._context, mockObject.Object, new JwtBearerOptions());
            context.Principal = principal;
            await _events.TokenValidated(context);

            Assert.False(context.Result.Succeeded);
        }

        public static IEnumerable<object[]> getInvalidClaims()
        {
            yield return new object[]
            {
                new Claim[]
                {
                    new Claim(ClaimTypes.Sid, "invalid"),
                    new Claim(ClaimTypes.Role, "1"),
                    new Claim("iat", 
                        Convert.ToUInt32(
                            DateTime.UtcNow
                                .Subtract(new DateTime(1970,1,1,0,0,0,DateTimeKind.Utc)).TotalSeconds
                            ).ToString())
                }
            };

            yield return new object[]
            {
                new Claim[]
                {
                    new Claim(ClaimTypes.Sid, "1"),
                    new Claim(ClaimTypes.Role, "invalid"),
                    new Claim("iat", 
                        Convert.ToUInt32(
                            DateTime.UtcNow
                                .Subtract(new DateTime(1970,1,1,0,0,0,DateTimeKind.Utc)).TotalSeconds
                            ).ToString())
                }
            };

            yield return new object[]
            {
                new Claim[]
                {
                    new Claim(ClaimTypes.Sid, "1"),
                    new Claim(ClaimTypes.Role, "1"),
                    new Claim("iat", "invalid")
                }
            };
        }

        private bool disposing = false;
        public void Dispose()
        {
            if (disposing)
                return;
            
            _dbContext.Dispose();
        }
    }
}