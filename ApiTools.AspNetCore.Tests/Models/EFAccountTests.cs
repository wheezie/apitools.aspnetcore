using System.Linq;
using Xunit;

namespace ApiTools.AspNetCore.Tests.Models
{
    public class EFAccountTests
    {
        [Fact]
        public void StripCredentials()
        {
            var account = new XUnitDbContext().DbContext.Accounts.First();
            account.StripCredentials();

            Assert.Null(account.Password);
        }
    }
}