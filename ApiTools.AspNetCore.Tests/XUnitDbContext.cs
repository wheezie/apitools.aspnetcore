using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

using ApiTools.AspNetCore.EF;
using ApiTools.AspNetCore.EF.Models;

namespace ApiTools.AspNetCore.Tests
{
    public class XUnitDbContext : IDisposable
    {
        public AppDbContext DbContext { get; private set; }

        private readonly SqliteConnection _conn;
        private bool IsDisposing = false;
        public XUnitDbContext()
        {
            _conn = new SqliteConnection("DataSource=:memory:");
            _conn.Open();

            
            var builder = new DbContextOptionsBuilder<AppDbContext>();
            builder.UseSqlite(_conn);

            DbContext = new AppDbContext(builder.Options);
            DbContext.Database.EnsureDeleted();
            DbContext.Database.EnsureCreated();
            addTestContent(DbContext);
        }

        private static void addTestContent(AppDbContext dbContext)
        {
            var passHasher = XUnitPasswordHasher.GetHasher();

            AccountRole role = new AccountRole
            {
                Id = 1,
                Name = "Testrole",
                Default = true,
                Permissions = new List<AccountPermission>
                {
                    new AccountPermission
                    {
                        Id = "TEST_PERMISSION",
                        Name = "This is a description"
                    }
                }
            };
            dbContext.AccountRoles.Add(role);

            var firstAccount = new Account
            {
                Id = 1,
                Blocked = false,
                Created = DateTime.UtcNow,
                Email = "test@domain.tld",
                Username = "test",
                FirstName = "Test",
                LastName = "Account",
                Password = passHasher.GenerateHash("123456789012345678901234567890"),
                Role = role,
                Image = new Picture
                {
                    Path = "/valid.png",
                    Uploaded = DateTime.UtcNow,
                    UploaderForeignKey = 1,
                    Hidden = false,
                    Deleted = false
                }
            };
            dbContext.Accounts.Add(firstAccount);
            dbContext.SaveChanges();

            role = new AccountRole
            {
                Id = 2,
                Name = "Testrole 2",
                Default = false,
                Permissions = new List<AccountPermission>
                {
                    new AccountPermission
                    {
                        Id = "TEST_PERMISSION",
                        Name = "This is a description"
                    }
                }
            };
            dbContext.AccountRoles.Add(role);

            dbContext.Accounts.Add(new Account
            {
                Id = 2,
                Blocked = false,
                Created = DateTime.UtcNow,
                Email = "test2@domain.tld",
                Username = "test2",
                FirstName = "Test2",
                LastName = "Yeet",
                Password = passHasher.GenerateHash("123456789012345678901234567890"),
                Role = role
            });

            dbContext.Albums.Add(new Album
            {
                Id = 1,
                Title = "test",
                Description = "testing",
                Published = DateTime.UtcNow,
                Publisher = firstAccount,
                Pictures = new List<Picture>
                {
                    firstAccount.Image
                }
            });
            dbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (IsDisposing)
                return;

            DbContext.Dispose();
            _conn.Dispose();
        }
    }
}