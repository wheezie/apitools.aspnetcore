using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Xunit;

namespace ApiTools.AspNetCore.Tests
{
    public partial class ApiTokenHandlerTests
    {
        private readonly ApiTokenHandler _handler;
        private readonly IConfiguration _config;
        public ApiTokenHandlerTests()
        {
            IConfiguration config = new ConfigurationBuilder()
                .AddInMemoryCollection(getValidConfig())
                .Build();
            this._config = config.GetSection("ApiTools:Security:Jwt");
            this._handler = new ApiTokenHandler(config);
        }

        [Theory]
        [MemberData(nameof(getInvalidConfigsAndErrorMessages))]
        public void Configuration_Invalid_configuration(Dictionary<string, string> configData, string error)
        {
            IConfiguration config = new ConfigurationBuilder()
                .AddInMemoryCollection(configData)
                .Build();
            ArgumentException exception = Assert.Throws<ArgumentException>(() => { new ApiTokenHandler(config); });
            Assert.Equal(error, exception.Message);
        }

        [Fact]
        public void Configuration_Algorithm_default()
        {
            var configData = getValidConfig();
            configData.Remove("ApiTools:Security:Jwt:Algorithm");
            IConfiguration config = new ConfigurationBuilder()
                .AddInMemoryCollection(configData)
                .Build();
            ApiTokenHandler handler = new ApiTokenHandler(config);
            Assert.Equal(SecurityAlgorithms.HmacSha512, handler.Algorithm);
        }

        [Theory]
        [InlineData(SecurityAlgorithms.HmacSha512)]
        [InlineData(SecurityAlgorithms.HmacSha384)]
        [InlineData(SecurityAlgorithms.HmacSha256)]
        public void Configuration_Algorithm_valid(string algorithm)
        {
            var configData = getValidConfig();
            configData["ApiTools:Security:Jwt:Algorithm"] = algorithm;
            IConfiguration config = new ConfigurationBuilder()
                .AddInMemoryCollection(configData)
                .Build();
            ApiTokenHandler handler = new ApiTokenHandler(config);
            Assert.Equal(algorithm, handler.Algorithm);
        }

        public static Dictionary<string, string> getValidConfig() =>
            new Dictionary<string, string>
            {
                { "ApiTools:Security:Jwt:Audiences:0", "AUDIENCE" },
                { "ApiTools:Security:Jwt:Algorithm", "HS512" },
                { "ApiTools:Security:Jwt:Issuers:0", "ISSUER" },
                { "ApiTools:Security:Jwt:Secret", "123456789012345678901234567890" },
            };
        public static IEnumerable<object[]> getInvalidConfigsAndErrorMessages()
        {
            var configData = getValidConfig();
            configData.Remove("ApiTools:Security:Jwt:Secret");
            yield return new object[]
            {
                configData,
                "JWT Secret must be configured!"
            };

            configData = getValidConfig();
            configData["ApiTools:Security:Jwt:Secret"] = "   ";
            yield return new object[]
            {
                configData,
                "JWT Secret must be configured!"
            };

            configData = getValidConfig();
            configData["ApiTools:Security:Jwt:Secret"] = "1234567";
            yield return new object[]
            {
                configData,
                "JWT Secret must be more secure!"
            };

            configData = getValidConfig();
            configData.Remove("ApiTools:Security:Jwt:Audiences:0");
            yield return new object[]
            {
                configData,
                "JWT Audiences must have at least one entry!"
            };

            configData = getValidConfig();
            configData.Remove("ApiTools:Security:Jwt:Issuers:0");
            yield return new object[]
            {
                configData,
                "JWT Issuers must have at least one entry!"
            };
        }
    }
}