using System;
using System.Linq;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Xunit;

namespace ApiTools.AspNetCore.Tests
{
    public partial class ApiTokenHandlerTests
    {
        [Fact]
        public void Configuration_CreateToken_valid()
        {
            var identity = new ClaimsIdentity(
                new Claim[]
                {
                    new Claim(ClaimTypes.Sid, "1"),
                    new Claim(ClaimTypes.Role, "1"),
                    new Claim("iat", Convert.ToUInt32(DateTime.UtcNow.Subtract(new DateTime(1970,1,1,0,0,0,DateTimeKind.Utc)).TotalSeconds).ToString())
                });
            string token = this._handler.CreateToken(identity, 1);

            var jwtHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken; 
            ClaimsPrincipal principal = jwtHandler.ValidateToken(token, _handler.ValidationParameters, out securityToken);
            Assert.NotNull(securityToken);
            Assert.NotNull(principal);
            Assert.All(principal.Claims, c => identity.Claims.Contains(c));
        }

        [Fact]
        public void Configuration_Decode_valid_external_validated()
        {
            var identity = new ClaimsIdentity(
                new Claim[]
                {
                    new Claim(ClaimTypes.Sid, "1"),
                    new Claim(ClaimTypes.Role, "1")
                });
            string jwtToken = this._handler.CreateToken(identity, 1);

            var parameters = new TokenValidationParameters
            {
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this._config.GetValue<string>("Secret"))),
                ValidAudiences = this._config.GetSection("Audiences").Get<string[]>(),
                ValidIssuers = this._config.GetSection("Issuers").Get<string[]>(),
                ValidateIssuer = true,
                ValidateIssuerSigningKey = true,
                ValidateAudience = true
            };

            SecurityToken token;
            ClaimsPrincipal principal = new JwtSecurityTokenHandler()
                .ValidateToken(jwtToken, parameters, out token);

            foreach(Claim claim in principal.Claims)
            {
                Assert.Contains(claim, principal.Claims);
            }
        }
    }
}