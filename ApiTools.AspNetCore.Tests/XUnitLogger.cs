using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace ApiTools.AspNetCore.Tests
{
    public class XUnitLogger
    {
        public static ILogger<T> GetLogger<T>()
        {
            return new NullLogger<T>();
        }
    }
}