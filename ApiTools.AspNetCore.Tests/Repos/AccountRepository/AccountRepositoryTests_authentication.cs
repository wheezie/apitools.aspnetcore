using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

using ApiTools.AspNetCore.EF.Models;
using ApiTools.AspNetCore.Models;

namespace ApiTools.AspNetCore.Tests.Repos
{
    public partial class AccountRepositoryTests
    {
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public async void BlockAccount_id_valid(ulong id)
        {
            await this._repository.BlockAccount(id);

            var account = this._dbcontext.DbContext.Accounts.First(a => a.Id == id);
            Assert.True(account.Blocked);
        }

        [Fact]
        public void BlockAccount_id_invalid()
        {
            Assert.ThrowsAsync<ArgumentException>(() => this._repository.BlockAccount(0));
        }

        [Fact]
        public void BlockAccount_account_invalid()
        {
            Assert.ThrowsAsync<ArgumentException>(() => this._repository.BlockAccount(new Account { Id = 0 }));
        }


        [Theory]
        [InlineData(null, null)]
        [InlineData(null, "")]
        [InlineData(null, "   ")]
        public async void CheckAuthentication_invalid_Username(string username, string email)
        {
            ValidationResponse validation = await this._repository.CheckAuthentication(new AccountRequest { Username = username, Email = email });

            Assert.NotNull(validation);
            Assert.False(validation.Valid);
            Assert.Equal("Username", validation.BadField.Field);
            Assert.Equal(BadField.Required, validation.BadField.Error);
        }

        [Theory]
        [MemberData(nameof(getInvalidAuthenticationAccounts))]
        public async void CheckAuthentication_invalid_Credentials(AccountRequest request)
        {
            ValidationResponse validation = await this._repository.CheckAuthentication(request);

            Assert.NotNull(validation);
            Assert.False(validation.Valid);
        }

        [Theory]
        [MemberData(nameof(getValidAuthenticationAccounts))]
        public async void CheckAuthentication_valid(AccountRequest request)
        {
            ValidationResponse validation = await this._repository.CheckAuthentication(request);

            Assert.NotNull(validation);
            Assert.True(validation.Valid);

            Account account = Assert.IsType<Account>(validation.ModifiedValue);
            Account dbAccount = this._dbcontext.DbContext.Accounts.First(a => a.Email == request.Email || a.Username == request.Username);
            Assert.Equal(dbAccount.Id, account.Id);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public async void SignoutEverywhere_id_valid(ulong id)
        {
            DateTime dt = this._dbcontext.DbContext.Accounts
                .Where(a => a.Id == id)
                .Select(a => a.ExpireTokensBefore).FirstOrDefault();
            await this._repository.SignoutEverywhere(id);
            
            DateTime updatedDt = this._dbcontext.DbContext.Accounts
                .Where(a => a.Id == id)
                .Select(a => a.ExpireTokensBefore).FirstOrDefault();
            Assert.NotEqual(dt, updatedDt);
            Assert.True(dt < updatedDt);
        }

        [Fact]
        public void SignoutEverywhere_id_invalid()
        {
            Assert.ThrowsAsync<ArgumentException>(() => this._repository.SignoutEverywhere(0));
        }

        public static IEnumerable<object[]> getInvalidAuthenticationAccounts()
        {
            yield return new object[]
            {
                new AccountRequest
                {
                    Email = "invalid@nonexistant.tld",
                    Username = "invalid",
                    Password = "notfunctioning"
                }
            };

            yield return new object[]
            {
                new AccountRequest
                {
                    Email = "test@domain.tld",
                    Username = "test",
                    Password = "notfunctioning"
                }
            };

            yield return new object[]
            {
                new AccountRequest
                {
                    Email = "test@domain.tld",
                    Password = "notfunctioning"
                }
            };

            yield return new object[]
            {
                new AccountRequest
                {
                    Username = "test",
                    Password = "notfunctioning"
                }
            };
        }

        public static IEnumerable<object[]> getValidAuthenticationAccounts()
        {
            yield return new object[]
            {
                new AccountRequest
                {
                    Email = "test@domain.tld",
                    Username = "test",
                    Password = "123456789012345678901234567890"
                }
            };

            yield return new object[]
            {
                new AccountRequest
                {
                    Email = "test@domain.tld",
                    Password = "123456789012345678901234567890"
                }
            };

            yield return new object[]
            {
                new AccountRequest
                {
                    Username = "test",
                    Password = "123456789012345678901234567890"
                }
            };

            yield return new object[]
            {
                new AccountRequest
                {
                    Username = "test2",
                    Password = "123456789012345678901234567890"
                }
            };
        }
    }
}