using System.Linq;
using Xunit;

namespace ApiTools.AspNetCore.Tests.Repos
{
    public partial class AccountRepositoryTests
    {
        [Fact]
        public void GetAccountQueryable_id_valid()
        {
            var account = this._repository.GetAccountQueryable(1)
                .FirstOrDefault();
            
            Assert.NotNull(account);
            Assert.Equal((ulong)1, account.Id);
        }

        [Fact]
        public void GetAccountQueryable_id_nonexistant()
        {
            Assert.Null(this._repository.GetAccountQueryable(0)
                .FirstOrDefault());
        }

        [Theory]
        [InlineData("test@domain.tld", "test", 1)]
        [InlineData("test2@domain.tld", "test2", 2)]
        public void GetAccountQueryable_usernameOrEmail_valid(string email, string username, ulong id)
        {
            var accountEmail = this._repository.GetAccountQueryable(email)
                .FirstOrDefault();
            var accountUsername = this._repository.GetAccountQueryable(username)
                .FirstOrDefault();

            Assert.NotNull(accountEmail);
            Assert.NotNull(accountUsername);

            Assert.Equal(id, accountUsername.Id);  
            Assert.Equal(id, accountEmail.Id);         
        }

        [Fact]
        public void GetAccountQueryable_usernameOrEmail_nonexistant()
        {
            Assert.Null(this._repository.GetAccountQueryable("nonexistant")
                .FirstOrDefault());
        }
    }
}