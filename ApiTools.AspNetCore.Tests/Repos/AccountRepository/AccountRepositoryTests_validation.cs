using System.Collections.Generic;
using System.Linq;
using Xunit;

using ApiTools.AspNetCore.Models;

namespace ApiTools.AspNetCore.Tests.Repos
{
    public partial class AccountRepositoryTests
    {
        [Theory]
        [MemberData(nameof(getBadAccountRequests))]
        public void ValidateAccount_invalid(AccountRequest request, BadField[] badFields)
        {
            BadField[] fields = this._repository.ValidateAccount(request);

            Assert.Equal(badFields.Length, fields.Length);
            foreach(var field in badFields) {
                var badField = fields.FirstOrDefault(f => f.Field == field.Field);
                Assert.NotNull(badField);
                Assert.Equal(field.Error, badField.Error);
            }
        }

        [Theory]
        [MemberData(nameof(getValidAccountRequests))]
        public void ValidateAccount_valid(AccountRequest request)
        {
            BadField[] fields = this._repository.ValidateAccount(request);

            Assert.Empty(fields);
        }


        public static IEnumerable<object[]> getBadAccountRequests()
        {
            yield return new object[]
            {
                new AccountRequest
                {
                    Email = null,
                    Username = null,
                    FirstName = null,
                    LastName = null,
                    Password = null
                },
                new BadField[]
                {
                    new BadField("Email", BadField.Required),
                    new BadField("Username", BadField.Required),
                    new BadField("FirstName", BadField.Required),
                    new BadField("LastName", BadField.Required),
                    new BadField("Password", BadField.Required)
                }
            };

            yield return new object[]
            {
                new AccountRequest
                {
                    Email = "",
                    Username = "",
                    FirstName = "",
                    LastName = "",
                    Password = ""
                },
                new BadField[]
                {
                    new BadField("Email", BadField.Required),
                    new BadField("Username", BadField.Required),
                    new BadField("FirstName", BadField.Required),
                    new BadField("LastName", BadField.Required),
                    new BadField("Password", BadField.Required)
                }
            };

            yield return new object[]
            {
                new AccountRequest
                {
                    Email = "  ",
                    Username = "  ",
                    FirstName = "  ",
                    LastName = "  ",
                    Password = "  "
                },
                new BadField[]
                {
                    new BadField("Email", BadField.Required),
                    new BadField("Username", BadField.Required),
                    new BadField("FirstName", BadField.Required),
                    new BadField("LastName", BadField.Required),
                    new BadField("Password", BadField.Required)
                }
            };

            yield return new object[]
            {
                new AccountRequest
                {
                    Email = "test@asdasd.",
                    Username = "testU",
                    FirstName = "First",
                    LastName = "Name",
                    Password = "P4#s"
                },
                new BadField[]
                {
                    new BadField("Email", BadField.Invalid)
                }
            };

            yield return new object[]
            {
                new AccountRequest
                {
                    Email = "test@domain.tld",
                    Username = "tes",
                    FirstName = "Fir",
                    LastName = "Nam",
                    Password = "Pas"
                },
                new BadField[]
                {
                    new BadField("Username", BadField.ToShort),
                    new BadField("FirstName", BadField.ToShort),
                    new BadField("LastName", BadField.ToShort),
                    new BadField("Password", BadField.ToShort)
                }
            };

            yield return new object[]
            {
                new AccountRequest
                {
                    Email = "test@domain.tld",
                    Username = "testtestttt",
                    FirstName = "testtestttt",
                    LastName = "testtestttt",
                    Password = "testt4s#ttt"
                },
                new BadField[]
                {
                    new BadField("Username", BadField.ToLong),
                    new BadField("FirstName", BadField.ToLong),
                    new BadField("LastName", BadField.ToLong)
                }
            };

            yield return new object[]
            {
                new AccountRequest
                {
                    Email = "test@domain.tld",
                    Username = "testU",
                    FirstName = "testU",
                    LastName = "testU",
                    Password = "ttss#"
                },
                new BadField[]
                {
                    new BadField("Password", BadField.RequiresDigits)
                }
            };

            yield return new object[]
            {
                new AccountRequest
                {
                    Email = "test@domain.tld",
                    Username = "testU",
                    FirstName = "testU",
                    LastName = "testU",
                    Password = "tt4s4"
                },
                new BadField[]
                {
                    new BadField("Password", BadField.RequiresSpecials)
                }
            };
        }
        public static IEnumerable<object[]> getValidAccountRequests()
        {
            yield return new object[]
            {
                new AccountRequest
                {
                    Email = "valid@domain.tld",
                    Username = "valid",
                    FirstName = "First",
                    LastName = "Name",
                    Password = "Y33#x"
                }
            };

            yield return new object[]
            {
                new AccountRequest
                {
                    Email = "valid2@domain.tld",
                    Username = "valid1",
                    FirstName = "First",
                    LastName = "Name",
                    Password = "asd3#x"
                }
            };
        }
    }
}