using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Xunit;
using Xunit.Abstractions;

using ApiTools.AspNetCore.Repos;

namespace ApiTools.AspNetCore.Tests.Repos
{
    public partial class AccountRepositoryTests
    {
        private readonly XUnitDbContext _dbcontext;
        private readonly AccountRepository _repository;
        private readonly ApiPasswordHasher _passHasher;
        private readonly ITestOutputHelper _output;
        public AccountRepositoryTests(ITestOutputHelper output)
        {
            this._output = output;
            this._dbcontext = new XUnitDbContext();

            var dict = new Dictionary<string, string>
            {
                { "ApiTools:Repositories:Account:Email:Required", "true" },

                { "ApiTools:Repositories:Account:Username:Required", "true" },
                { "ApiTools:Repositories:Account:Username:Minimum", "4" },
                { "ApiTools:Repositories:Account:Username:Maximum", "10" },

                { "ApiTools:Repositories:Account:FirstName:Required", "true" },
                { "ApiTools:Repositories:Account:FirstName:Minimum", "4" },
                { "ApiTools:Repositories:Account:FirstName:Maximum", "10" },

                { "ApiTools:Repositories:Account:LastName:Required", "true" },
                { "ApiTools:Repositories:Account:LastName:Minimum", "4" },
                { "ApiTools:Repositories:Account:LastName:Maximum", "10" },

                { "ApiTools:Repositories:Account:Password:Minimum", "4" },
                { "ApiTools:Repositories:Account:Password:Number", "true" },
                { "ApiTools:Repositories:Account:Password:SpecialCharacter", "true" }
            };

            var config = new ConfigurationBuilder()
                .AddInMemoryCollection(dict)
                .Build();

            this._passHasher = XUnitPasswordHasher.GetHasher();

            this._repository = new AccountRepository(
                this._dbcontext.DbContext, config,
                XUnitLogger.GetLogger<AccountRepository>(), this._passHasher);
        }
    }
}