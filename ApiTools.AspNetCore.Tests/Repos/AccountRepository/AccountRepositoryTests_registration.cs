using System.Collections.Generic;
using System.Linq;
using Xunit;

using ApiTools.AspNetCore.Models;

namespace ApiTools.AspNetCore.Tests.Repos
{
    public partial class AccountRepositoryTests
    {
        [Theory]
        [MemberData(nameof(getInvalidRegisterAccounts))]
        public async void RegisterAccount_invalid(AccountRequest request)
        {
            Assert.Null(await this._repository.RegisterAccount(request));
        }

        [Fact]
        public async void RegisterAccount_invalid_notindb()
        {
            var request = new AccountRequest
            {
                Email = "nonexistant@domain.tld",
                Username = "nonexistant",
                Password = "123456789012345678901234567890"
            };
            await this._repository.RegisterAccount(request);
            Assert.False(this._dbcontext.DbContext.Accounts.Any(a => a.Email == request.Email));
        }

        [Fact]
        public async void RegisterAccount_valid()
        {
            var request = new AccountRequest
            {
                Email = "valid10@domain.tld",
                Username = "User10",
                FirstName = "User",
                LastName = "Name",
                Password = "Justtestin"
            };
            var account = await this._repository.RegisterAccount(request);
            Assert.NotNull(account);

            Assert.Equal(request.Username, account.Username);
            Assert.Equal(request.Email, account.Email);
            Assert.Equal(request.FirstName, account.FirstName);
            Assert.Equal(request.LastName, account.LastName);

            var dbAccount = this._dbcontext.DbContext.Accounts.First(a => a.Id == account.Id);
            Assert.True(this._passHasher.Verify(request.Password, dbAccount.Password));
        }

        [Fact]
        public async void RegisterAccount_strippedcredentials()
        {
            var request = new AccountRequest
            {
                Email = "valid10@domain.tld",
                Username = "User10",
                FirstName = "User",
                LastName = "Name",
                Password = "Justtestin"
            };
            var account = await this._repository.RegisterAccount(request);
            Assert.NotNull(account);

            Assert.Null(account.Password);
        }

        public static IEnumerable<object[]> getInvalidRegisterAccounts()
        {
            yield return new object[]
            {
                null
            };

            yield return new object[]
            {
                new AccountRequest()
            };

            yield return new object[]
            {
                new AccountRequest
                {
                    Password = "asd"
                }
            };

            yield return new object[]
            {
                new AccountRequest
                {
                    Email = "asd@asdas.sad",
                    Password = "asd"
                }
            };
        }
    }
}