using System;
using System.Linq;
using Xunit;

using ApiTools.AspNetCore.EF.Models;

namespace ApiTools.AspNetCore.Tests.Repos
{
    public partial class AccountRepositoryTests
    {
        [Fact]
        public async void UpdateDetachedAccount_valid()
        {
            var accountOld = this._dbcontext.DbContext.Accounts.FirstOrDefault(a => a.Id == 1);
            this._dbcontext.DbContext.Entry(accountOld).State = Microsoft.EntityFrameworkCore.EntityState.Detached;
            var account = new Account
            {
                Id = 1,
                Username = "unchanged",
                Email = "unchanged@othetdomain.tld",
                FirstName = "changed",
                LastName = "changed",
                Description = "changed",
                Password = "unchanged",
                ExpireTokensBefore = DateTime.UtcNow
            };

            await this._repository.UpdateDetachedAccount(account);
            
            var accountNew = this._dbcontext.DbContext.Accounts.FirstOrDefault(a => a.Id == 1);

            Assert.Equal(account.FirstName, accountNew.FirstName);
            Assert.Equal(account.LastName, accountNew.LastName);
            Assert.Equal(account.Description, accountNew.Description);
            Assert.Equal(account.ExpireTokensBefore, accountNew.ExpireTokensBefore);

            // IsModified is set to false for these, so they will be reset to their original values
            Assert.Equal(accountOld.Username, accountNew.Username);
            Assert.Equal(accountOld.Email, accountNew.Email);
            Assert.Equal(accountOld.Password, accountNew.Password);
        }

        [Fact]
        public async void UpdateDetachedAccount_invalid()
        {
            await Assert.ThrowsAsync<ArgumentException>(() => this._repository.UpdateDetachedAccount(new Account { Id = 0 }));
        }

        [Fact]
        public async void UpdateAccount_valid()
        {
            var accountOld = this._dbcontext.DbContext.Accounts.FirstOrDefault(a => a.Id == 1);
            accountOld.Username = "unchanged";
            accountOld.Email = "unchanged@othetdomain.tld";
            accountOld.FirstName = "changed";
            accountOld.LastName = "changed";
            accountOld.Description = "changed";
            accountOld.Password = "unchanged";
            accountOld.ExpireTokensBefore = DateTime.UtcNow;

            await this._repository.UpdateAccount(accountOld);
            
            var accountNew = this._dbcontext.DbContext.Accounts.FirstOrDefault(a => a.Id == 1);

            Assert.Equal(accountOld.FirstName, accountNew.FirstName);
            Assert.Equal(accountOld.LastName, accountNew.LastName);
            Assert.Equal(accountOld.Description, accountNew.Description);
            Assert.Equal(accountOld.ExpireTokensBefore, accountNew.ExpireTokensBefore);

            // IsModified is set to false for these, so they will be reset to their original values
            Assert.Equal(accountOld.Username, accountNew.Username);
            Assert.Equal(accountOld.Email, accountNew.Email);
            Assert.Equal(accountOld.Password, accountNew.Password);
        }

        [Fact]
        public async void UpdateAccount_invalid()
        {
            await Assert.ThrowsAsync<ArgumentException>(() => this._repository.UpdateAccount(new Account { Id = 1 }));
            await Assert.ThrowsAsync<ArgumentException>(() => this._repository.UpdateAccount(new Account { Id = 0 }));
        }
    }
}