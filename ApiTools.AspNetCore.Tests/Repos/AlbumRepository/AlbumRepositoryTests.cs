using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Xunit.Abstractions;

using ApiTools.AspNetCore.Repos;

namespace ApiTools.AspNetCore.Tests.Repos
{
    public partial class AlbumRepositoryTests
    {
        private readonly XUnitDbContext _dbcontext;
        private readonly IAlbumRepository _repository;
        private readonly ITestOutputHelper _output;
        public AlbumRepositoryTests(ITestOutputHelper output)
        {
            this._output = output;
            this._dbcontext = new XUnitDbContext();

            var dict = new Dictionary<string, string>
            {
                { "ApiTools:Repositories:Album:Title:Required", "true" },
                { "ApiTools:Repositories:Album:Title:Minimum", "2" },
                { "ApiTools:Repositories:Album:Title:Maximum", "10" },

                { "ApiTools:Repositories:Album:Description:Required", "false" },
                { "ApiTools:Repositories:Album:Description:Minimum", "2" },
                { "ApiTools:Repositories:Album:Description:Maximum", "10" },
            };

            var config = new ConfigurationBuilder()
                .AddInMemoryCollection(dict)
                .Build();

            this._repository = new AlbumRepository(
                this._dbcontext.DbContext,
                XUnitLogger.GetLogger<AlbumRepository>(),
                config);
        }
    }
}