using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

using ApiTools.AspNetCore.EF.Models;

namespace ApiTools.AspNetCore.Tests.Repos
{
    public partial class AlbumRepositoryTests
    {
        [Fact]
        public async void AddAlbum_invalid()
        {
            Assert.False(await this._repository.AddAlbum(null));
            Assert.False(await this._repository.AddAlbum(new Album()));
        }

        [Theory]
        [MemberData(nameof(validAlbums))]
        public async void AddAlbum_valid(Album album)
        {
            Assert.True(await this._repository.AddAlbum(album));

            Assert.Equal(this._dbcontext.DbContext.Albums.FirstOrDefault(a => a.Id == album.Id), album);
        }

        public static IEnumerable<object[]> validAlbums()
        {
            yield return new object[]
            {
                new Album
                {
                    Title = "123",
                    Description = "123"
                }
            };
            yield return new object[]
            {
                new Album
                {
                    Title = "",
                    Description = ""
                }
            };
        }
    }
}