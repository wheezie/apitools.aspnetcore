using System;
using System.Linq;
using Xunit;

using ApiTools.AspNetCore.EF.Models;

namespace ApiTools.AspNetCore.Tests.Repos
{
    public partial class AlbumRepositoryTests
    {
        [Fact]
        public async void UpdateAlbum_invalid()
        {
            await Assert.ThrowsAsync<ArgumentException>(() => this._repository.UpdateAlbum(null));
            await Assert.ThrowsAsync<ArgumentException>(() => this._repository.UpdateAlbum(new Album()));
            await Assert.ThrowsAsync<ArgumentException>(() => this._repository.UpdateAlbum(new Album { Id = 0 }));
        }

        [Fact]
        public async void UpdateAlbum_valid()
        {
            DateTime dtNow = DateTime.UtcNow;
            var album = this._repository.GetAlbumQueryable(1).First();
            album.Description = "changed";
            album.Title = "changed";
            album.Publisher = this._dbcontext.DbContext.Accounts.First(a => a.Id == 2);
            album.Published = dtNow;

            await this._repository.UpdateAlbum(album);

            Assert.Equal("changed", album.Description);
            Assert.Equal("changed", album.Title);
            Assert.NotEqual((ulong)2, album.Publisher.Id);
            Assert.NotEqual(dtNow, album.Published);
        }

        [Fact]
        public async void UpdateDetachedAlbum_invalid()
        {
            await Assert.ThrowsAsync<ArgumentException>(() => this._repository.UpdateDetachedAlbum(null));
            await Assert.ThrowsAsync<ArgumentException>(() => this._repository.UpdateDetachedAlbum(new Album()));
            await Assert.ThrowsAsync<ArgumentException>(() => this._repository.UpdateDetachedAlbum(new Album { Id = 0 }));
        }

        [Fact]
        public async void UpdateDetachedAlbum_valid()
        {
            DateTime dtNow = DateTime.UtcNow;
            var album = this._repository.GetAlbumQueryable(1).First();
            album.Description = "changed";
            album.Title = "changed";
            album.Publisher = this._dbcontext.DbContext.Accounts.First(a => a.Id == 2);
            album.Published = dtNow;

            await this._repository.UpdateDetachedAlbum(album);

            Assert.Equal("changed", album.Description);
            Assert.Equal("changed", album.Title);
            Assert.NotEqual((ulong)2, album.Publisher.Id);
            Assert.NotEqual(dtNow, album.Published);
        }
    }
}