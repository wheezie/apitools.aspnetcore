using System.Collections;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Xunit;
using Xunit.Abstractions;

using ApiTools.AspNetCore.EF.Models;
using ApiTools.AspNetCore.Models;
using ApiTools.AspNetCore.Repos;

namespace ApiTools.AspNetCore.Tests.Repos
{
    public class AlbumRepositoryTests_validation
    {
        private readonly ITestOutputHelper _output;
        public AlbumRepositoryTests_validation(ITestOutputHelper output)
        {
            this._output = output;
        }

        [Theory]
        [MemberData(nameof(validAlbumAndConfigs))]
        public void ValidateAlbum_valid(Dictionary<string, string> configDict, Album album)
        {
            using (var dbContext = new XUnitDbContext())
            {
                var repo = new AlbumRepository(
                    dbContext.DbContext,
                    XUnitLogger.GetLogger<AlbumRepository>(),
                    new ConfigurationBuilder()
                        .AddInMemoryCollection(configDict)
                        .Build());

                Assert.Null(repo.ValidateAlbum(album));
            }
        }

        [Theory]
        [MemberData(nameof(invalidAlbumAndConfigs))]
        public void ValidateAlbum_invalid(Dictionary<string, string> configDict, Album album, BadField[] badFields)
        {
            using (var dbContext = new XUnitDbContext())
            {
                var repo = new AlbumRepository(
                    dbContext.DbContext,
                    XUnitLogger.GetLogger<AlbumRepository>(),
                    new ConfigurationBuilder()
                        .AddInMemoryCollection(configDict)
                        .Build());

                var fields = repo.ValidateAlbum(album);

                Assert.NotNull(fields);
                Assert.All(badFields, bf => fields.Any(f => bf.Error == f.Error && bf.Field == f.Field));
            }
        }

        public static IEnumerable<object[]> validAlbumAndConfigs()
        {
            var dict = new Dictionary<string, string>
                {
                    { "ApiTools:Repositories:Album:Title:Required", "false" },
                    { "ApiTools:Repositories:Album:Title:Minimum", "2" },
                    { "ApiTools:Repositories:Album:Title:Maximum", "10" },

                    { "ApiTools:Repositories:Album:Description:Required", "false" },
                    { "ApiTools:Repositories:Album:Description:Minimum", "2" },
                    { "ApiTools:Repositories:Album:Description:Maximum", "10" },
                };
            yield return new object[]
            {
                dict,
                new Album
                {
                    Title = null,
                    Description = null
                }
            };
            yield return new object[]
            {
                dict,
                new Album
                {
                    Title = "",
                    Description = ""
                }
            };
            yield return new object[]
            {
                dict,
                new Album
                {
                    Title = "  ",
                    Description = "  "
                }
            };

            var requiredDict = new Dictionary<string, string>
                {
                    { "ApiTools:Repositories:Album:Title:Required", "false" },
                    { "ApiTools:Repositories:Album:Title:Minimum", "2" },
                    { "ApiTools:Repositories:Album:Title:Maximum", "10" },

                    { "ApiTools:Repositories:Album:Description:Required", "false" },
                    { "ApiTools:Repositories:Album:Description:Minimum", "2" },
                    { "ApiTools:Repositories:Album:Description:Maximum", "10" },
                };
            yield return new object[]
            {
                requiredDict,
                new Album
                {
                    Title = "12",
                    Description = "12"
                }
            };
            yield return new object[]
            {
                requiredDict,
                new Album
                {
                    Title = "12345",
                    Description = "12345"
                }
            };
            yield return new object[]
            {
                requiredDict,
                new Album
                {
                    Title = "1234567890",
                    Description = "1234567890"
                }
            };
        }
        public static IEnumerable<object[]> invalidAlbumAndConfigs()
        {
            var dict = new Dictionary<string, string>
                {
                    { "ApiTools:Repositories:Album:Title:Required", "true" },
                    { "ApiTools:Repositories:Album:Title:Minimum", "2" },
                    { "ApiTools:Repositories:Album:Title:Maximum", "10" },

                    { "ApiTools:Repositories:Album:Description:Required", "true" },
                    { "ApiTools:Repositories:Album:Description:Minimum", "2" },
                    { "ApiTools:Repositories:Album:Description:Maximum", "10" },
                };
            yield return new object[]
            {
                dict,
                new Album
                {
                    Title = null,
                    Description = null
                },
                new BadField[]
                {
                    new BadField("Title", BadField.Required),
                    new BadField("Description", BadField.Required),
                }
            };
            yield return new object[]
            {
                dict,
                new Album
                {
                    Title = "",
                    Description = ""
                },
                new BadField[]
                {
                    new BadField("Title", BadField.Required),
                    new BadField("Description", BadField.Required),
                }
            };
            yield return new object[]
            {
                dict,
                new Album
                {
                    Title = "    ",
                    Description = "    "
                },
                new BadField[]
                {
                    new BadField("Title", BadField.Required),
                    new BadField("Description", BadField.Required),
                }
            };
            yield return new object[]
            {
                dict,
                new Album
                {
                    Title = "1",
                    Description = "1"
                },
                new BadField[]
                {
                    new BadField("Title", BadField.ToShort),
                    new BadField("Description", BadField.ToShort),
                }
            };
            yield return new object[]
            {
                dict,
                new Album
                {
                    Title = "12345678901",
                    Description = "12345678901"
                },
                new BadField[]
                {
                    new BadField("Title", BadField.ToLong),
                    new BadField("Description", BadField.ToLong),
                }
            };
        }
    }
}