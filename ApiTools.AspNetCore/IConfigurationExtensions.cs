using System;
using Microsoft.Extensions.Configuration;

namespace ApiTools.AspNetCore
{
    public static class IConfigurationExtensions
    {
        public static int GetLimitedInt(this IConfiguration config, string key, int min, int max, int defaultValue = 0)
        {
            int value = config.GetValue<int>(key, defaultValue);
            return (value > max ? max : (value < min ? min : value));
        }
    }
}