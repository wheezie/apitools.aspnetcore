using Microsoft.AspNetCore.Mvc;

using ApiTools.AspNetCore.Models;

namespace ApiTools.AspNetCore
{
    public class ApiControllerBase : ControllerBase
    {
        /// <summary>
        /// Retrieve the BadRequest response with one field
        /// </summary>
        /// <param name="field">Field name</param>
        /// <param name="error">Error type</param>
        /// <returns>The response object</returns>
        public BadRequestObjectResult BadRequestResponse(string field, string error)
        {
            return BadRequest(new BadRequestResponse(field, error));
        }

        /// <summary>
        /// Retrieve the BadRequest response for the bad fields
        /// </summary>
        /// <param name="badFields">BadField or list of</param>
        /// <returns>The response object</returns>
        public BadRequestObjectResult BadRequestResponse(params BadField[] badFields)
        {
            return BadRequest(new BadRequestResponse(badFields));
        }
    }
}