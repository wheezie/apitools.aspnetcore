using System.Runtime.CompilerServices;
using System;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using ApiTools.AspNetCore.EF;
using ApiTools.AspNetCore.EF.Models;
using ApiTools.AspNetCore.Models;

namespace ApiTools.AspNetCore.Repos
{
    public interface IAccountRepository
    {
        Task BlockAccount(ulong id);
        Task BlockAccount(Account account);
        Task<ValidationResponse> CheckAuthentication(AccountRequest request);
        IQueryable<Account> GetAccountQueryable(ulong id, bool track = true);
        IQueryable<Account> GetAccountQueryable(string usernameOrEmail, bool track = true);
        Task UpdateAccount(Account account);
        Task UpdateDetachedAccount(Account account);
        Task<ValidationResponse> UpdatePassword(Account account, string newPassword);
        Task<Account> RegisterAccount(AccountRequest request);
        Task SignoutEverywhere(ulong id);
        Task SignoutEverywhere(Account account);
        BadField[] ValidateAccount(AccountRequest account);
    }

    public class AccountRepository : IAccountRepository
    {
        private IConfigurationSection _config;
        private readonly AppDbContext _dbContext;
        private readonly ILogger<AccountRepository> _logger;
        private readonly ApiPasswordHasher _passHasher;

        /// <summary>
        /// Account repository for easy access
        /// </summary>
        /// <param name="dbContext">(DI) Application dbContext</param>
        /// <param name="config">(DI) Application configuration</param>
        /// <param name="logger">(DI) Logging endpoint</param>
        public AccountRepository(AppDbContext dbContext, IConfiguration config, ILogger<AccountRepository> logger, ApiPasswordHasher passHasher)
        {
            this._config = config.GetSection("ApiTools:Repositories:Account");
            this._dbContext = dbContext;
            this._logger = logger;
            this._passHasher = passHasher;
        }

        /// <summary>
        /// Block an account based of it's identifier
        /// </summary>
        /// <param name="id">Account identifier</param>
        public async Task BlockAccount(ulong id)
        {
            Account account = await  this._dbContext.Accounts.FirstOrDefaultAsync(a => a.Id == id);
            await BlockAccount(account);
        }

        /// <summary>
        /// Block an account based off it's class
        /// </summary>
        /// <param name="account">Account instance</param>
        public async Task BlockAccount(Account account)
        {
            if (account == null)
                throw new ArgumentException("Specified account is invalid.");

            try {
                account.Blocked = true;
                account.BlockedDate = DateTime.UtcNow;
                this._dbContext.Update(account);
                await this._dbContext.SaveChangesAsync();
            } catch {
                this._logger.LogWarning("Attempted to block an account failed.");
                throw new ArgumentException("No account found by id.");
            }
        }

        /// <summary>
        /// Verify authentication of credentials.
        /// </summary>
        /// <param name="accountRequest">credentials request</param>
        /// <returns>The response containing the verification state & account (if valid)</returns>
        public async Task<ValidationResponse> CheckAuthentication(AccountRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Username))
            {
                if (string.IsNullOrWhiteSpace(request.Email))
                    return new ValidationResponse("Username", BadField.Required);
            }

            Account account = await this._dbContext.Accounts
                .FirstOrDefaultAsync(a => (a.Email == request.Email || a.Username == request.Username) && !a.Blocked);

            if (account == null || !_passHasher.Verify(request.Password, account.Password))
                return new ValidationResponse();

            return new ValidationResponse(account);
        }

        /// <summary>
        /// Retrieve an account queryable
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <param name="track">Should track entity (default: true)</param>
        /// <returns>IQueryable account</returns>
        public IQueryable<Account> GetAccountQueryable(ulong id, bool track = true)
        {
            var query = this._dbContext.Accounts.Where(a => a.Id == id);
            if (!track)
                return query.AsNoTracking();

            return query;
        }

        /// <summary>
        /// Retrieve an account queryable
        /// </summary>
        /// <param name="usernameOrEmail">Username or email</param>
        /// <param name="track">Should track entity (default: true)</param>
        /// <returns>IQueryable account</returns>
        public IQueryable<Account> GetAccountQueryable(string usernameOrEmail, bool track = true)
        {
            var query = this._dbContext.Accounts.Where(a => a.Username.ToLower() == usernameOrEmail.ToLower() || a.Email == usernameOrEmail.ToLower());
            if (!track)
                return query.AsNoTracking();

            return query;
        }

        /// <summary>
        /// Update an account instance (Does not update email, username or password!)
        /// </summary>
        /// <param name="account">Reference to update</param>
        public async Task UpdateAccount(Account account)
        {
            try {
                this._dbContext.Update(account);

                this._dbContext.Entry(account)
                    .State = EntityState.Modified;
                this._dbContext.Entry(account)
                    .Property(o => o.Password)
                    .IsModified = false;
                this._dbContext.Entry(account)
                    .Property(o => o.Email)
                    .IsModified = false;
                this._dbContext.Entry(account)
                    .Property(o => o.Username)
                    .IsModified = false;


                await this._dbContext.SaveChangesAsync();
            } catch {
                throw new ArgumentException("Provided account isn't tracked by the context");
            }
        }

        /// <summary>
        /// Update an account using a non-tracked instance of that entity.
        /// </summary>
        /// <param name="account">Reference to update</param>
        public async Task UpdateDetachedAccount(Account account)
        {
            try {
                Account currentAccount = await this._dbContext.Accounts.FirstAsync(a => a.Id == account.Id);
                this._dbContext.Entry(currentAccount)
                    .CurrentValues.SetValues(account);

                await this.UpdateAccount(currentAccount);
            } catch {
                throw new ArgumentException("Provided account is invalid or another entry is tracked by the context");
            }
        }

        /// <summary>
        /// Verify and update an account's new password
        /// </summary>
        /// <param name="account">Target account</param>
        /// <param name="newPassword">New password</param>
        public async Task<ValidationResponse> UpdatePassword(Account account, string newPassword)
        {
            var config = this._config.GetSection("Password");
            var field = ValidationTools.ValidatePasswordString(
                newPassword,
                "Password",
                config.GetLimitedInt("Minimum", 2, 32, 6),
                config.GetValue<bool>("Number", true),
                config.GetValue<bool>("SpecialCharacter", true));

            if (!field.Valid)
                return field;

            account.Password = this._passHasher.GenerateHash((string)field.ModifiedValue);
            this._dbContext.Update(account);
            await this._dbContext.SaveChangesAsync();
            return new ValidationResponse(account);
        }

        /// <summary>
        /// Register an account.
        /// </summary>
        /// <param name="account">Request information</param>
        /// <returns>The created account, or null if failed</returns>
        public async Task<Account> RegisterAccount(AccountRequest accountRequest)
        {
            if (accountRequest == null || string.IsNullOrWhiteSpace(accountRequest.Password))
                return null;

            Account account = new Account
            {
                Username = accountRequest.Username,
                Email = accountRequest.Email,
                FirstName = accountRequest.FirstName,
                LastName = accountRequest.LastName,
            };

            account.Password = _passHasher.GenerateHash(accountRequest.Password);

            try {
                this._dbContext.Add(account);
                if (await this._dbContext.SaveChangesAsync() <= 0)
                    return null;
            } catch (Exception ex) {
                this._logger.LogError(ex, "Couldn't register account: {Mail}, {Username}", account.Email, account.Username);
                return null;
            }

            this._logger.LogInformation("Created account {Id} for {Mail}", account.Id, account.Email);
            this._dbContext.Entry(account).State = EntityState.Detached;
            account.StripCredentials();
            return account;
        }

        /// <summary>
        /// Sign out all tokens everywhere
        /// </summary>
        /// <param name="id">Account identifier</param>
        public async Task SignoutEverywhere(ulong id)
        {
            Account account = await this._dbContext.Accounts.FirstOrDefaultAsync(a => a.Id == id);
            await SignoutEverywhere(account);
        }

        /// <summary>
        /// Sign out all tokens everywhere
        /// </summary>
        /// <param name="account">Account instance</param>
        public async Task SignoutEverywhere(Account account)
        {
            account.ExpireTokensBefore = DateTime.UtcNow;
            try {
                this._dbContext.Update(account);
                await this._dbContext.SaveChangesAsync();
            } catch {
                this._logger.LogWarning("Attempted to signout non-existing account.");
                throw new ArgumentException("Provided account does not exist.");
            }
        }
        /// <summary>
        /// Validate an account and return the incorrect fields.
        /// </summary>
        /// <param name="account">AccountRequest to validate</param>
        /// <returns>List of failed/Incorrect fields</returns>
        public BadField[] ValidateAccount(AccountRequest account)
        {
            if (account == null) {
                return new BadField[] { new BadField("account", BadField.Required) };
            }

            List<BadField> badFields = new List<BadField>();

            // First name
            IConfigurationSection config = this._config.GetSection("FirstName");
            ValidationResponse field = ValidationTools.ValidateString(
                account.FirstName,
                "FirstName",
                config.GetValue<bool>("Required", true),
                config.GetLimitedInt("Minimum", 2, 32, 2),
                config.GetLimitedInt("Maximum", 2, 32, 32));
            if (!field.Valid)
                badFields.Add(field.BadField);
            else
                account.FirstName = (string)field.ModifiedValue;

            // Last name
            config = this._config.GetSection("LastName");
            field = ValidationTools.ValidateString(
                account.LastName,
                "LastName",
                config.GetValue<bool>("Required", true),
                config.GetLimitedInt("Minimum", 2, 32, 2),
                config.GetLimitedInt("Maximum", 2, 32, 32));
            if (!field.Valid)
                badFields.Add(field.BadField);
            else
                account.LastName = (string)field.ModifiedValue;

            // Username
            config = this._config.GetSection("Username");
            field = ValidationTools.ValidateString(
                account.Username,
                "Username",
                config.GetValue<bool>("Required", true),
                config.GetLimitedInt("Minimum", 2, 32, 4),
                config.GetLimitedInt("Maximum", 2, 32, 32));
            if (!field.Valid)
                badFields.Add(field.BadField);
            else
                account.Username = (string)field.ModifiedValue;

            // Email
            config = this._config.GetSection("Email");
            if (config.GetValue<bool>("Required", true)) {
                field = ValidationTools.ValidateEmailString(account.Email, "Email");
                if (!field.Valid)
                    badFields.Add(field.BadField);
                else
                    account.Email = (string)field.ModifiedValue;
            }

            // Password
            config = this._config.GetSection("Password");
            field = ValidationTools.ValidatePasswordString(
                account.Password,
                "Password",
                config.GetLimitedInt("Minimum", 2, 32, 6),
                config.GetValue<bool>("Number", true),
                config.GetValue<bool>("SpecialCharacter", true));
            if (!field.Valid)
                badFields.Add(field.BadField);
            else
                account.Password = (string)field.ModifiedValue;

            return badFields.ToArray();
        }
    }
}