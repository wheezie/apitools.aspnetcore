using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using ApiTools.AspNetCore.EF;
using ApiTools.AspNetCore.EF.Models;
using ApiTools.AspNetCore.Models;

namespace ApiTools.AspNetCore.Repos
{
    public interface IAlbumRepository
    {
        /// <summary>
        /// Create a new album
        /// </summary>
        /// <param name="album">Album contents</param>
        /// <returns>The succeeded state of the request</returns>
        Task<bool> AddAlbum(Album album);
        /// <summary>
        /// Get an album queryable selecting by id
        /// </summary>
        /// <param name="albumId">Album id</param>
        /// <param name="track">Should track in repo (default: true)</param>
        /// <returns>An IQueryable of the account</returns>
        IQueryable<Album> GetAlbumQueryable(ulong albumid, bool track = true);
        /// <summary>
        /// Updates the provided album (only if tracked by the repo)
        /// </summary>
        /// <param name="album">(Tracked) album to update</param>
        Task UpdateAlbum(Album album);
        /// <summary>
        /// Updates a detached album into the context
        /// </summary>
        /// <param name="album">Album contents to update</param>
        Task UpdateDetachedAlbum(Album album);
        /// <summary>
        /// Validate an album against config requirements
        /// </summary>
        /// <param name="album">Album contents</param>
        /// <returns>A list of bad fields (if any)</returns>
        BadField[] ValidateAlbum(Album album);
    }
    public class AlbumRepository : IAlbumRepository
    {
        private readonly AppDbContext _dbContext;
        private readonly ILogger<AlbumRepository> _logger;
        private readonly IConfiguration _config;
        public AlbumRepository(AppDbContext dbContext, ILogger<AlbumRepository> logger, IConfiguration config)
        {
            this._dbContext = dbContext;
            this._logger = logger;
            this._config = config.GetSection("ApiTools:Repositories:Album");
        }

        public async Task<bool> AddAlbum(Album album)
        {
            try {
                album.Id = 0;
                this._dbContext.Albums.Add(album);
                return (await this._dbContext.SaveChangesAsync() > 0);
            } catch (Exception ex) {
                this._logger.LogError(ex, "An error occurred attempting to add an album");
                return false;
            }
        }

        public IQueryable<Album> GetAlbumQueryable(ulong albumId, bool track = true)
        {
            var query = this._dbContext.Albums.Where(a => a.Id == albumId);
            if (!track)
                return query.AsNoTracking();

            return query;
        }

        public async Task UpdateAlbum(Album album)
        {
            try {
                this._dbContext.Update(album);

                this._dbContext.Entry(album)
                    .Reference(o => o.Publisher)
                    .IsModified = false;
                this._dbContext.Entry(album)
                    .Property(o => o.Published)
                    .IsModified = false;

                await this._dbContext.SaveChangesAsync();
            } catch {
                throw new ArgumentException("Provided album isn't tracked by the context");
            }
        }

        public async Task UpdateDetachedAlbum(Album album)
        {
            try {
                Album currentAlbum = this._dbContext.Albums.First(o => o.Id == album.Id);
                this._dbContext.Entry(currentAlbum)
                    .CurrentValues.SetValues(album);

                await this._dbContext.SaveChangesAsync();
            } catch {
                throw new ArgumentException("Provided album couldn't be found by the provided id");
            }
        }

        public BadField[] ValidateAlbum(Album album)
        {
            List<BadField> badFields = new List<BadField>();
            IConfigurationSection config = this._config.GetSection("Title");
            ValidationResponse field = ValidationTools.ValidateString(
                album.Title,
                "Title",
                config.GetValue<bool>("Required", true),
                config.GetLimitedInt("Minimum", 2, 48, 4),
                config.GetLimitedInt("Maximum", 2, 48, 48));
            if (!field.Valid)
                badFields.Add(field.BadField);
            else
                album.Title = (string)field.ModifiedValue;

            config = this._config.GetSection("Description");
            field = ValidationTools.ValidateString(
                album.Description,
                "Description",
                config.GetValue<bool>("Required", true),
                config.GetLimitedInt("Minimum", 2, 65535, 4),
                config.GetLimitedInt("Maximum", 2, 65535, 60000));
            if (!field.Valid)
                badFields.Add(field.BadField);
            else
                album.Description = (string)field.ModifiedValue;

            if (badFields.Count > 0)
                return badFields.ToArray();
            return null;
        }
    }
}