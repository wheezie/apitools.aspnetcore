using System.Collections.Generic;
using System;
using System.Diagnostics;
using Microsoft.Extensions.Configuration;

using BCrypt.Net;

namespace ApiTools.AspNetCore
{
    public class ApiPasswordHasher
    {
        private readonly int _factor;
        private readonly bool _enhanced;
        private readonly HashType _type;

        /// <summary>
        /// Api Password hashing wrapper
        /// </summary>
        /// <param name="config">(DI) configuration file</param>
        public ApiPasswordHasher(IConfiguration config)
        {
            config = config.GetSection("ApiTools:Security:Password");

            switch(config.GetValue<string>("Algorithm", "").ToUpper()) {
                case "SHA256":
                    this._type = HashType.SHA256;
                    break;
                default:
                case "SHA384":
                    _type = HashType.SHA384;
                    break;
                case "SHA512":
                    _type = HashType.SHA512;
                    break;
            }
            _factor = config.GetValue<int>("Factor", 12);
            _enhanced = config.GetValue<bool>("Enhanced", true);
        }

        /// <summary>
        /// BCrypt hash the provided password
        /// </summary>
        /// <param name="password">Input password</param>
        /// <returns>Generated password hash</returns>
        public string GenerateHash(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(
                password, BCrypt.Net.BCrypt.GenerateSalt(_factor),
                _enhanced, _type);
        }

        /// <summary>
        /// Verify the password against the hash.
        /// </summary>
        /// <param name="password">Input password</param>
        /// <param name="hashPassword">Password hash</param>
        /// <returns>Validity of the password</returns>
        public bool Verify(string password, string hashPassword)
        {
            return BCrypt.Net.BCrypt.Verify(password, hashPassword, _enhanced, _type);
        }

        /// <summary>
        /// Benchmark bcrypt performance using your hardware.
        /// </summary>
        /// /// <param name="maxFactor">Maximum factor to start benchmarking from</param>
        /// <param name="minFactor">Factor to stop benchmarking after</param>
        /// <returns>List of benchmark results</returns>
        public Dictionary<int, TimeSpan> BenchmarkFactor(int maxFactor = 20, int minFactor = 4)
        {
            Dictionary<int, TimeSpan> result = new Dictionary<int, TimeSpan>();
            Stopwatch sw = Stopwatch.StartNew();
            for(int factor = maxFactor; factor > minFactor; factor--)
            {
                sw.Restart();
                BCrypt.Net.BCrypt.HashPassword(
                    "RwiKnN>9xg3*C)1AZl.)y8f_:GCz,vt3T]PI", BCrypt.Net.BCrypt.GenerateSalt(factor), _enhanced, _type);
                sw.Stop();

                result.Add(factor, sw.Elapsed);
            }

            return result;
        }
    }
}