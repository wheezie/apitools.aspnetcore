using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ApiTools.AspNetCore
{
    public static class ApiTokenExtensions
    {
        /// <summary>
        /// Register authentication with the JwtBearer and the ApiTokenHandler
        /// </summary>
        /// <param name="services">Source DI IServiceCollectionn</param>
        /// <param name="config">Application collection</param>
        /// <returns>The modified DI IServiceCollection</returns>
        public static IServiceCollection AddApiTokenHandling(this IServiceCollection services, IConfiguration config)
        {
            ApiTokenHandler tokenHandler = new ApiTokenHandler(config);
            services.AddSingleton<ApiTokenHandler>(tokenHandler);

            services.AddAuthentication(o => {
                o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(o => {
                    o.TokenValidationParameters = tokenHandler.ValidationParameters;
                    o.EventsType = typeof(ApiAuthenticationEvents);
                });

            services.AddScoped<ApiAuthenticationEvents>();
            return services;
        }
    }
}