using System.Linq;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using ApiTools.AspNetCore.EF;
using ApiTools.AspNetCore.EF.Models;

namespace ApiTools.AspNetCore.Auth
{
    public class ApiAuthorizationHandler : AuthorizationHandler<ApiPermissionRequirement>
    {
        private readonly ILogger<ApiAuthorizationHandler> _logger;
        private readonly AppDbContext _dbContext;

        public ApiAuthorizationHandler(ILogger<ApiAuthorizationHandler> logger, AppDbContext dbContext)
        {
            this._logger = logger;
            this._dbContext = dbContext;
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, ApiPermissionRequirement requirement)
        {
            var claim = context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
            if (claim != null && uint.TryParse(claim.Value, out uint roleId)
                && await this._dbContext.AccountPermissions.AnyAsync(p => p.AccountRoleId == roleId && p.Id == requirement.Permission)) {
                context.Succeed(requirement);
            }
        }
    }
}