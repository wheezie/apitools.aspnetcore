using Microsoft.AspNetCore.Authorization;

namespace ApiTools.AspNetCore
{
    public class ApiPermissionAttribute : AuthorizeAttribute
    {
        public string Permission
        {
            get
            {
                return Policy.Substring(ApiAuthPolicyProvider.POLICY_PREFIX.Length);
            }
            set
            {
                Policy = $"{ApiAuthPolicyProvider.POLICY_PREFIX}{value.ToString()}";
            }
        }

        public ApiPermissionAttribute(string permission) => Permission = permission;
    }

    public class ApiPermissionRequirement : IAuthorizationRequirement
    {
        public string Permission { get; private set; }

        public ApiPermissionRequirement(string permission) => Permission = permission;
    }
}