using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace ApiTools.AspNetCore
{
    public class ApiTokenHandler
    {
        public TokenValidationParameters ValidationParameters { get; private set; }

        private readonly IConfigurationSection _config;
        private readonly SigningCredentials _credentials;
        private readonly JwtSecurityTokenHandler _tokenHandler;
        public ApiTokenHandler(IConfiguration config)
        {
            this._config = config.GetSection("ApiTools:Security:Jwt");

            string secret = this._config.GetValue<string>("Secret");
            if (string.IsNullOrWhiteSpace(secret))
                throw new ArgumentException("JWT Secret must be configured!");
            else if (secret.Length < 8)
                throw new ArgumentException("JWT Secret must be more secure!");

            string[] Audiences = this._config.GetSection("Audiences").Get<string[]>();
            if (Audiences == null || Audiences.Length <= 0)
                throw new ArgumentException("JWT Audiences must have at least one entry!");

            string[] Issuers = this._config.GetSection("Issuers").Get<string[]>();
            if (Issuers == null || Issuers.Length <= 0)
                throw new ArgumentException("JWT Issuers must have at least one entry!");

            this.ValidationParameters = new TokenValidationParameters
            {
                ValidateLifetime = true,
                ValidateAudience = true,
                ValidAudiences = Audiences,
                ValidateIssuer = true,
                ValidIssuers = Issuers,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret))
            };

            this._credentials = new SigningCredentials(
                this.ValidationParameters.IssuerSigningKey,
                this._config.GetValue<string>("Algorithm", SecurityAlgorithms.HmacSha512));
            this._tokenHandler = new JwtSecurityTokenHandler();
        }

        /// <summary>
        /// Create a JSON Web Token
        /// </summary>
        /// <param name="identity">The account's claims identity</param>
        /// <param name="days">Token lifetime days (default = 3)</param>
        /// <returns>The generated JWT token string (Base64-encoded)</returns>
        public string CreateToken(ClaimsIdentity identity, uint accountId, uint days = 3)
        {
            Claim claim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Sid);
            if (claim != null)
                identity.RemoveClaim(claim);
            identity.AddClaim(new Claim(ClaimTypes.Sid, accountId.ToString()));

            SecurityTokenDescriptor token = new SecurityTokenDescriptor
            {
                Subject = identity,
                Expires = DateTime.Now.AddDays(days),
                Issuer = _config.GetValue<string>("Issuers:0"),
                Audience =  _config.GetValue<string>("Audiences:0"),
                SigningCredentials = _credentials
            };

            return _tokenHandler.CreateEncodedJwt(token);
        }

        /// <summary>
        /// Decode the token string
        /// </summary>
        /// <param name="jwtToken">JWT token to decode</param>
        /// <param name="validate">Verify the token before decoding</param>
        /// <returns>The jwt security token with string contents</returns>
        public JwtSecurityToken DecodeToken(string jwtToken, bool validate = false)
        {
            if (validate && !ValidateToken(jwtToken))
                return null;
            
            return _tokenHandler.ReadJwtToken(jwtToken);
        }

        /// <summary>
        /// Validate the token & lifetime
        /// </summary>
        /// <param name="jwtToken">JWT token string</param>
        /// <returns>The validity of lifetime of the JWT token.</returns>
        public bool ValidateToken(string jwtToken)
        {
            SecurityToken token;
            try {
                _tokenHandler.ValidateToken(jwtToken, ValidationParameters, out token);
            } catch {
                return false;
            }

            if (token == null)
                return false;

            return token.ValidFrom < DateTime.UtcNow && token.ValidTo > DateTime.UtcNow;
        }

        public string Algorithm 
        {
            get { return this._credentials.Algorithm; }
        }
    }
}