namespace ApiTools.AspNetCore.Models
{
    public partial class BadField
    {
        public const string Required = "required";
        public const string Invalid = "invalid";
        public const string ToLong = "to_long";
        public const string ToShort = "to_short";
        public const string RequiresDigits = "requires_digits";
        public const string RequiresSpecials = "requires_specials";
    }
}