using System;

namespace ApiTools.AspNetCore.Models
{
    public class Profile
    {
        /// <summary>
        /// Profile account identifier
        /// </summary>
        public ulong Id { get; set; }
        /// <summary>
        /// First name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Last name
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Role name
        /// </summary>
        public string RoleName { get; set; }
        /// <summary>
        /// Image url
        /// </summary>
        public string Image { get; set; }
    }
}