using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ApiTools.AspNetCore
{
    public static class ApiBasicsExtensions
    {
        public static IServiceCollection AddApiBasics(this IServiceCollection services, IConfiguration config)
        {
            services.AddSingleton<ApiPasswordHasher>();
            services.AddSingleton<IAuthorizationPolicyProvider, ApiAuthPolicyProvider>();
            return services;
        }
    }
}