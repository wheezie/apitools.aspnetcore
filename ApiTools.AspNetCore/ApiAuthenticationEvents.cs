using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;

using ApiTools.AspNetCore.EF;

namespace ApiTools.AspNetCore
{
    public class ApiAuthenticationEvents: JwtBearerEvents
    {
        private readonly AppDbContext _dbContext;
        public ApiAuthenticationEvents(AppDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        /// <summary>
        /// Validates the token and it's claims against the database.
        /// </summary>
        /// <param name="context">Token validation context</param>
        public override async Task TokenValidated(TokenValidatedContext context)
        {
            Claim idClaim = context.Principal.Claims
                .FirstOrDefault(c => c.Type == ClaimTypes.Sid);
            Claim roleClaim = context.Principal.Claims
                .FirstOrDefault(c => c.Type == ClaimTypes.Role);
            Claim issuedClaim = context.Principal.Claims
                .FirstOrDefault(c => c.Type == "iat");

            DateTime issuedDate;
            uint roleId;
            ulong accountId;
            if (idClaim != null && roleClaim != null && issuedClaim != null
                && ulong.TryParse(idClaim.Value, out accountId)
                && uint.TryParse(roleClaim.Value, out roleId)) {
                try {
                    issuedDate = new DateTime(1970,1,1,0,0,0,DateTimeKind.Utc)
                        .AddSeconds(Convert.ToUInt32(issuedClaim.Value));
                } catch {
                    context.Fail(new UnauthorizedAccessException());
                    return;
                }

                // Check account validity
                if (await _dbContext.Accounts.AnyAsync(a => 
                    a.Id == accountId && !a.Blocked
                    && a.ExpireTokensBefore < issuedDate
                    && a.Role != null && a.Role.Id == roleId)) {
                    context.Success();
                    return;
                }
            }

            context.Fail(new UnauthorizedAccessException());
        }
    }
}